package com.example.hernan.demoreceivercall;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumber, txtName, txtCorreo;
    private ImageButton imgBtnPhone, imgBtnName, imgBtnCamera, imgBtnCorreo;
    private Button btnContactos;
    private final int PHONE_CALL_CODE = 100; //id para request de PHONE
    private final int CAMERA_CODE = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumber = findViewById(R.id.txtNumber);
        txtName = findViewById(R.id.txtName);
        txtCorreo = findViewById(R.id.txtCorreo);
        imgBtnPhone = findViewById(R.id.imgBtnPhone);
        imgBtnName = findViewById(R.id.imgBtnName);
        imgBtnCamera = findViewById(R.id.imgBtnCamera);
        imgBtnCorreo = findViewById(R.id.imgBtnCorreo);
        btnContactos = findViewById(R.id.btnContactos);


        //Boton para Telefono
        //Otra forma de llamar (Telefono v2)- Sin permisos, el usuario solo daria click para llamar
        //Intent iPhone = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:220149"));
        //startActivity(iPhone);
        imgBtnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = txtNumber.getText().toString();
                if (phone != null && !phone.isEmpty()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    //comprobar version actual de android que estamos corriendo
                    //Preguntamos si nuestra version es mayor a la version M (Mashmallow 6.x.x)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //COMPROBAR SI SE ACEPTO, NO ACEPTO, NUNCA SE HA PREGUNTADO
                        if (checkPermission(Manifest.permission.CALL_PHONE)) {
                            //Acepto
                            Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            startActivity(i);
                        } else {
                            //NO ACEPTO, NUNCA SE HA PREGUNTADO
                            if (!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                                //Si nunca se le ha preguntado
                                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE); //Pregunta persmisos en tiempo de ejecucion, este metodo requiere como minimo la API 23, metodo async
                            } else {
                                //No acepto
                                Toast.makeText(MainActivity.this, "Por favor, habilite el permiso de télefono", Toast.LENGTH_LONG).show();
                                Intent intentSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intentSettings.addCategory(Intent.CATEGORY_DEFAULT);
                                intentSettings.setData(Uri.parse("package:" + getPackageName()));
                                //Los flag son para aplicar metodos despues de un evento.
                                //Un claro ejemplo es para cuando vamos atras (en Login)
                                intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentSettings.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intentSettings.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                startActivity(intentSettings);
                            }
                        }
                    } else {
                        OlderVersions(phone);
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Debe ingresar un número válido", Toast.LENGTH_LONG).show();
                }
            }

            //Para versiones antiguas
            private void OlderVersions(String phone) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                if (checkPermission(Manifest.permission.CALL_PHONE)) {
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "No tiene permisos", Toast.LENGTH_LONG).show();
                }
            }
        });

        //Boton para Web
        imgBtnName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = txtName.getText().toString();
                if (url != null && !url.isEmpty()) {
                    Intent iWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
                    startActivity(iWeb);
                }
            }
        });

        //Boton para Enviar Correo
        imgBtnCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txtCorreo.getText().toString();
                if (email != null && !email.isEmpty()) {
                    //Forma rapida
                    //Intent iCorreo = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"+ correo));
                    //Forma completa
                    Intent iEmail = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:" + email));
                    iEmail.setType("plain/text");
                    iEmail.putExtra(Intent.EXTRA_SUBJECT, "Mi Tema");
                    iEmail.putExtra(Intent.EXTRA_TEXT, "Hola estoy aprendiendo Android desde 0");
                    iEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{email, "juan@gmail.com"});
                    //Forzar a que siempre se pregunte!!
                    //startActivity(Intent.createChooser(iEmail, "Escoge cliente de correos"));
                    startActivity(iEmail);
                }
            }
        });

        //Boton para Contactos
        btnContactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent icontacts = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
                startActivity(icontacts);
            }
        });

        //Boton para Camara
        imgBtnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iCamera = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(iCamera, CAMERA_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.toUri(0);
                    Toast.makeText(this, "Resultado" + result, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Ha ocurrido un error con la Imagen", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    //Metodo async
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //requestCode -> codigo que mandamos aqui(como un id)
        switch (requestCode) {
            case PHONE_CALL_CODE:
                //peticion del telefono
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)) {
                    //comprobar si se acepta o se rechaza la peticion.
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //Permiso aceptado
                        String phone = txtNumber.getText().toString();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            //Por ultimo comprobamos si el permiso fue concedido,
                            //bloque denegado
                            return;
                        }
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Rechazo el permiso", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }


    }

    private boolean checkPermission(String permissions) {
        int result = this.checkCallingOrSelfPermission(permissions);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}